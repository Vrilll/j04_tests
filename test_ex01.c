/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex01.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/09 16:31:41 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/09 16:33:43 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int factorial_result;

	factorial_result = ft_recursive_factorial(16);
	printf("%d", factorial_result);
	return (0);
}
