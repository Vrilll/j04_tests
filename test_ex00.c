/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex00.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/09 15:40:59 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/10 18:06:02 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int	factorial_result;

	factorial_result = ft_iterative_factorial(0);
	printf("%d", factorial_result);
	return (0);
}
