/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ex02.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/10 15:11:12 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/10 18:15:38 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	main(void)
{
	int result;

	result = ft_iterative_power(3018, -3869);
	printf("%d\n", result);
	return (0);
}
